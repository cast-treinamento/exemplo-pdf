

/* requires itextpdf-5.1.2.jar or similar */

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import java.io.File;

public class Teste {

    public static void main(String args[]) throws Exception {
        //Loading an existing document
        File file = new File("/home/nataniel/teste/read.pdf");
        PDDocument doc = PDDocument.load(file);

        //Retrieving the page
        PDPage page = doc.getPage(0);

        //Creating PDImageXObject object
        PDImageXObject pdImage = PDImageXObject.createFromFile("/home/nataniel/teste/caixa.png",doc);

        //creating the PDPageContentStream object
        PDPageContentStream contents = new PDPageContentStream(doc, page, true, true);

        //Drawing the image in the PDF document
        pdImage.setHeight(20);
        pdImage.setWidth(20);
        contents.drawImage(pdImage, 0, 0);

        System.out.println("Image inserted");

        //Closing the PDPageContentStream object
        contents.close();

        //Saving the document
        doc.save("/home/nataniel/teste/output.pdf");

        //Closing the document
        doc.close();

    }

}
